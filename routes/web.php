<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
})->name('home');

Route::get('currencies', 'CurrencyController@index')->name('currencies.index');
Route::post('currencies', 'CurrencyController@store')->name('currencies.store');
Route::get('currencies/add', 'CurrencyController@add')->name('currencies.add');
Route::get('currencies/{currency}', 'CurrencyController@show')->name('currencies.show');
Route::delete('currencies/{currency}', 'CurrencyController@destroy')->name('currencies.destroy');
Route::put('currencies/{currency}', 'CurrencyController@update')->name('currencies.update');
Route::get('currencies/{currency}/edit', 'CurrencyController@edit')->name('currencies.edit');

