### How to set up? ###

* Installation:
	* install docker and docker-compose
	* cd in projectdirectory
	* `$ cp docker-compose.example.yml docker-compose.yml`
	* In the docker-compose.yml file, change only the port numbers if you have such ports already in use
	* `$ docker-compose up -d`
	* `$ cp .env.example .env`
	* In the .env file, type the accesses to the database, such as in the docker-compose.yml file + (DB_HOST=mysql && DB_PORT=3306)
	* Craete folder /storage/docker for MySQL
	* `$ docker-compose exec php-fpm composer install`
	* `$ docker-compose exec php-fpm php artisan migrate`
	* `$ docker-compose exec php-fpm php artisan db:seed`
	* `$ npm i && npm watch`
* Done...