@extends('layout')

@section('title', $currency->title)

@section('content')
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2>{{$currency->title}}</h2>
            <ol class="breadcrumb">
                <li>
                    <a href="{{ route('home') }}">Home</a>
                </li>
                <li>
                    <a href="{{ route('currencies.index') }}">Currencies</a>
                </li>
                <li class="active">
                    <strong>{{ $currency->title }}</strong>
                </li>
            </ol>
        </div>
        <div class="col-xs-6 col-md-4 m-t-md">
            <a href="{{ route('currencies.edit', $currency->id) }}" class="btn btn-warning  pull-left m-r-md edit-button">Edit</a>
            <form method="POST" action="{{route('currencies.destroy', ['id'=>$currency->id])}}">
                <input name="_method" type="hidden" value="DELETE">
                <input name="_token" type="hidden" value="{{csrf_token()}}">
                <button type="submit" class="btn btn-danger delete-button">Delete</button>
            </form>
        </div>
    </div>
    <div class="row wrapper wrapper-content animated fadeInRight">
        <div class="col-lg-6">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Common</h5>
                    <div class="ibox-tools">
                        <a class="collapse-link">
                            <i class="fa fa-chevron-up"></i>
                        </a>
                    </div>
                </div>
                <div class="ibox-content">
                    <table class="table table-striped table-bordered white-bg">
                        <tbody>
                        <tr>
                            <td><strong>Logo</strong></td>
                            <td><img src="{{ $currency->logo_url}}" /></td>
                        </tr>
                        <tr>
                            <td><strong>Name</strong></td>
                            <td>{!! $currency->title !!}  </td>
                        </tr>
                        <tr>
                            <td><strong>Short name</strong></td>
                            <td>{!! $currency->short_name !!}</td>
                        </tr>
                        <tr>
                            <td><strong>Price USD</strong></td>
                            <td>{{ $currency->price }}</td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection